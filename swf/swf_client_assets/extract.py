#! /usr/bin/python3
# encoding: utf8

import os
import json
from PIL import Image, ImageOps

if not os.path.exists('output'):
	os.mkdir('output')

for root, dirs, files in os.walk('defs'):
	for f in files:
		# Prepare files
		basename = f.split('.')[0]
		if not os.path.isfile(f'images/sheet_{basename}.png'):
			continue
		with open(f'defs/{f}') as def_file:
			def_sheet = json.loads(def_file.read())
		img_sheet = Image.open(f'images/sheet_{basename}.png')
		outdir = 'output/' + basename
		if not os.path.exists(outdir):
			os.mkdir(outdir)
		regions_processed = set()

		# Process JSON
		for sprite in def_sheet['sheet'][0]['slice']:
			if '_ignore' in sprite: # See key _comment
				continue
			s_id = sprite['id']
			print(f"Processing {basename}/{s_id}…")
			params = [int(x) for x in sprite['params'].split(',')]

			# Animated sprites
			if 'anim' in sprite:
				anim_range = [int(x) for x in sprite['anim'][0]['range'].split(',')]
				anim_vertical = True
				anim_ofs = params[2]
				if len(params) == 6 and params[4] == 1:
					anim_vertical = False
					anim_ofs = params[3]
				pos = params[0:4]
				pos[2] = pos[0] + pos[2]
				pos[3] = pos[1] + pos[3]
				print(pos)

				# Go through each frame
				for i in range(0, len(anim_range)):
					r = anim_range[i]
					try:
						if anim_vertical:
							frame = (pos[0] + anim_ofs * r, pos[1], pos[2] + anim_ofs * r, pos[3])
							print(f"  Anim frame {i}: {frame}")
							img = img_sheet.crop(frame)
							if 'flipX' in sprite and sprite['flipX']:
								img = ImageOps.mirror(img)
							img.save(f'{outdir}/{s_id}@{i}.png')
						else:
							frame = (pos[0], pos[1] + anim_ofs * r, pos[2], pos[3] + anim_ofs * r)
							print(f"  Anim frame {i}: {frame}")
							img = img_sheet.crop(frame)
							if 'flipX' in sprite and sprite['flipX']:
								img = ImageOps.mirror(img)
							img.save(f'{outdir}/{s_id}@{i}.png')
						regions_processed.add(frame)
					except:
						print("\t\033[91mCould not save file " + f"{s_id}@{i} from {basename}" + "\033[0m")
						if os.path.isfile(f'{outdir}/{s_id}@{i}.png'):
							os.remove(f'{outdir}/{s_id}@{i}.png')
			# Static sprite
			else:
				# Variants
				if len(params) == 5 and params[4] > 1:
					variants = [(x, f'#{x}') for x in range(0, params[4])]
				else:
					variants = [(0, '')]
				variant_ofs = params[2]

				for v in variants:
					pos = params[0:4]
					pos[0] += variant_ofs * v[0]
					pos[2] = pos[0] + pos[2]
					pos[3] = pos[1] + pos[3]
					if v[1]:
						print(f"  Static variant {v[1]}: {pos}")
					try:
						img = img_sheet.crop(pos)
						if 'flipX' in sprite and sprite['flipX']:
							img = ImageOps.mirror(img)
						img.save(f'{outdir}/{s_id}{v[1]}.png')
						regions_processed.add(tuple(pos))
					except:
						print("\t\033[91mCould not save file " + f"{s_id}{v[1]} from {basename}" + "\033[0m")
						if os.path.isfile(f'{outdir}/{s_id}{v[1]}.png'):
							os.remove(f'{outdir}/{s_id}{v[1]}.png')

		# Isolate unregistered sprites
		# (needs to be done in a second pass bc some IDs use the same region or a sub-region)
		print(f"Cleaning up sheet {basename}…")
		for region in regions_processed:
			img_sheet.paste((255, 255, 255, 0), region)
		img_sheet.save(f'output/{basename}-unregistered_sprites.png')